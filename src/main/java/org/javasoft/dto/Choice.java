package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

@Root
@ToString
@Convert(value = Choice.ChoiceConverter.class)
public class Choice {

    @Text(required = false)
    private String text;

    @Element(name = "image")
    private Image image;

    public Image getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    static class ChoiceConverter implements Converter<Choice> {

        private final Serializer serializer = new Persister();

        @Override
        public Choice read(InputNode inputNode) throws Exception {
            Choice choice = new Choice();
            final InputNode imageNode = inputNode.getNext("image");
            if(imageNode != null){
                choice.image = serializer.read(Image.class,imageNode);
            }
            choice.text = inputNode.getValue();
            System.out.println(">>>>>>> " + inputNode);
            return choice;
        }

        @Override
        public void write(OutputNode outputNode, Choice choice) throws Exception {
            outputNode.setValue(choice.text);
            serializer.write(choice.image,outputNode);
        }
    }
}
