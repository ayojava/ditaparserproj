package org.javasoft.dto;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;


public class Steps {

    @ElementList(name = "step")
    private List<Step> stepList;

    public List<Step> getStepList() {
        return stepList;
    }
}
