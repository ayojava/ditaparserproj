package org.javasoft.dto;

import org.simpleframework.xml.ElementList;
import java.util.List;

public class Metadata {

    @ElementList(name = "othermeta")
    private List<Othermeta> otherMetaList;

    public List<Othermeta> getOtherMetaList() {
        return otherMetaList;
    }
}
