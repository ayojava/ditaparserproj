package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Attribute;

@ToString
public class Image {

    @Attribute(name = "placement")
    private String placement;

    @Attribute(name = "href")
    private String href;

    public String getPlacement() {
        return placement;
    }

    public String getHref() {
        return href;
    }
}
