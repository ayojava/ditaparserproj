package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Attribute;

@ToString
public class Othermeta {

    @Attribute
    private String name;

    @Attribute
    private String content;

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }
}
