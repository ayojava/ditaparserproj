package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root
@ToString
public class Step {

    @ElementList(required = false)
    private List<Choice> choices;

    public List<Choice> getChoices() {
        return choices;
    }

//    @Element(required = false)
//    private Choices choices;
//
//    public Choices getChoices() {
//        return choices;
//    }

    @Element(required = false)
    private Cmd cmd;

    public Cmd getCmd() {
        return cmd;
    }

    @Element(required = false)
    private String info;

    public String getInfo() {
        return info;
    }
}
