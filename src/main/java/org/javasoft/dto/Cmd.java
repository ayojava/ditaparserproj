package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

@Root
@ToString
@Convert(value = Cmd.CmdConverter.class)
public class Cmd {

    @Text
    private String text;

    @Element(name = "image", required = false)
    private Image image;

    public Image getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    static class CmdConverter implements Converter<Cmd>{

        private final Serializer serializer = new Persister();

        @Override
        public Cmd read(InputNode inputNode) throws Exception {
            Cmd cmd = new Cmd();
            cmd.text = inputNode.getValue();

            final InputNode imageNode = inputNode.getNext("image");
            if(imageNode != null){
                cmd.image = serializer.read(Image.class,imageNode);
            }
            return cmd;
        }

        @Override
        public void write(OutputNode outputNode, Cmd cmd) throws Exception {
            outputNode.setValue(cmd.text);
            serializer.write(cmd.image,outputNode);
        }
    }
}
