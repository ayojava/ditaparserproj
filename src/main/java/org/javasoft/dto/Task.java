package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@ToString
@Root(name = "task", strict = false)
public class Task {

    @Attribute(name = "xmlns:ditaarch" , required = false)
    private String ditaArch;

    @Attribute(name = "id")
    private String taskId;

    @Attribute(name = "xml:lang" , required = false)
    private String lang;

    @Attribute(name = "ditaarch:DITAArchVersion" , required = false)
    private String ditaVersion;

    @Element(name="title")
    private String title;

    @Element(name="taskbody")
    private TaskBody taskBody;

    @Element(name="prolog")
    private Prolog prolog;

    public Prolog getProlog() {
        return prolog;
    }

    public String getTitle() {
        return title;
    }

    public TaskBody getTaskBody() {
        return taskBody;
    }

    public String getDitaArch() {
        return ditaArch;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getLang() {
        return lang;
    }

    public String getDitaVersion() {
        return ditaVersion;
    }
}
