package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

@ToString
public class TaskBody {

    @ElementList
    private List<Step> steps;

    @Element(name = "context" , required = false)
    private Context context;

    public List<Step> getSteps() {
        return steps;
    }

    public Context getContext() {
        return context;
    }

}
