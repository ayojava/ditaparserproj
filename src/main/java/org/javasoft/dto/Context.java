package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;

@ToString
public class Context {

    @Element(name = "note", required = false)
    private Note note;

    public Note getNote() {
        return note;
    }
}
