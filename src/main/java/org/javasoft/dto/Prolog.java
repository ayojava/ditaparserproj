package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

@ToString
public class Prolog {
//
//    @Element(name = "metadata")
//    private Metadata metaData;
//
//    public Metadata getMetaData() {
//        return metaData;
//    }

    @ElementList
    private List<Othermeta> metadata;

    public List<Othermeta> getMetadata() {
        return metadata;
    }
}
