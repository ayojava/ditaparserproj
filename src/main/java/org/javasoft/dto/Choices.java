package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

//@Root
@ToString
public class Choices {

    //@ElementList(required = false)
    private List<Choice> choice;

    public List<Choice> getChoice() {
        return choice;
    }
}
