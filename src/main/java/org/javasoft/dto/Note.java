package org.javasoft.dto;

import lombok.ToString;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.convert.Convert;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

@Root
@ToString
@Convert(value = Note.NoteConverter.class)
public class Note {

    @Text
    private String text;

    @Element(name = "image")
    private Image image;

    public Image getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    static class NoteConverter implements Converter<Note> {

        private final Serializer serializer = new Persister();

        @Override
        public Note read(InputNode inputNode) throws Exception {
            final InputNode imageNode = inputNode.getNext("image");
            Note note = new Note();
            if(imageNode != null){
                note.image = serializer.read(Image.class,imageNode);
            }
            note.text = inputNode.getValue();
            return note;
        }

        @Override
        public void write(OutputNode outputNode, Note note) throws Exception {
            outputNode.setValue(note.text);
            serializer.write(note.image,outputNode);
        }
    }
}
