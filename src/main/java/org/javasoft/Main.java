package org.javasoft;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.dto.Task;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;

public class Main {

    private String inputFilePath ="/Users/ayodeji.ilori/Documents/intellijProj/DitaParserProj/src/main/resources/in/";
    private String outputFilePath ="/Users/ayodeji.ilori/Documents/intellijProj/DitaParserProj/src/main/resources/out/";

    String sample1DitaFile= "sample1.dita";

    String sample2DitaFile = "sample2.dita";

    String sample3DitaFile = "sample3.dita";

    String sample4DitaFile = "sample4.dita";

    public Task handleSample1() throws Exception {
        return showTaskObj(sample1DitaFile);
    }

    //Sample2 has this in the dita file - <info>All load labels must be intact.</info>
    public Task handleSample2() throws Exception {
        return showTaskObj(sample2DitaFile);
    }

    //Sample3 has this in the dita file -
    //          <context>
    //              <note>
    //                <image href="d60cde5e-2880-43d6-a1ea-a5ce618f8cc1.png" placement="break"/>
    //                Collect and treat waste oil properly and according to local hazardous waste oil regulations.
    //              </note>
    //            </context>
    public Task handleSample3() throws Exception {
        return showTaskObj(sample3DitaFile);
    }

    public Task handleSample4() throws Exception {
        return showTaskObj(sample4DitaFile);
    }

    private Task showTaskObj(String fileName) throws Exception{
        Serializer serializer = new Persister(new AnnotationStrategy());
        final File targetFile = changeFileExtension(fileName);
        final Task taskObj = serializer.read(Task.class, targetFile);
        System.out.println("Task Object \n " +taskObj);
        return taskObj;
    }

    private File changeFileExtension(String sourceFileName) throws Exception {
        final String source = StringUtils.substring(sourceFileName, 0, sourceFileName.length() - 5);
        File sourceFile= new File(inputFilePath,sourceFileName);
        String target = source + ".xml";
        File targetFile =  new File(outputFilePath,target);
        if(sourceFile.exists()){
            sourceFile.renameTo(targetFile);
        }
        return targetFile;
    }

    public static void main(String[] args) throws Exception {
        final Main main = new Main();
        main.handleSample4();
    }
}
