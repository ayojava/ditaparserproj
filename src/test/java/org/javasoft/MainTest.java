package org.javasoft;

import org.apache.commons.lang3.StringUtils;
import org.javasoft.dto.Context;
import org.javasoft.dto.Note;
import org.javasoft.dto.Task;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    private static Main main;

    @BeforeAll
    static void setUp() throws Exception {
        main = new Main();
    }

    @Test
    void handleSample1() throws Exception {
        final Task task = main.handleSample1();
        assertNotNull( task,"Task object is null");
        assertNotNull(task.getTaskId(),"Task object is null"  );
        assertFalse(task.getTaskId().isEmpty(),"TaskId is Empty" );
        assertTrue(StringUtils.equalsIgnoreCase(task.getTaskId(),"task.dita_ab1d07ad-a35a-42ee-9260-2eef3fa0c8b9"),"TaskId is the required string");
        assertNotNull(task.getProlog(),"prolog object is null" );
        assertTrue(task.getProlog().getMetadata().size() == 9 ,"Size of Metadata List must be 9" );
        assertNotNull(task.getTaskBody(),"TaskBody object is null" );
        assertTrue(task.getTaskBody().getSteps().size() == 3 ,"Size of steps List must be 3" );
    }

    @Test
    void handleSample2() throws Exception {
        final Task task = main.handleSample2();
        assertNotNull( task,"Task object is null");
        task.getTaskBody().getSteps().forEach(step -> {
            assertNotNull(step.getInfo(),"Info is null");
            assertTrue(StringUtils.equalsIgnoreCase("All load labels must be intact.",step.getInfo()),"Invalid text for info");
        });
    }

    @Test
    void handleSample3() throws Exception {
        final Task task = main.handleSample3();
        assertNotNull( task,"Task object is null");
        final Context context = task.getTaskBody().getContext();
        assertNotNull(context,"Context object is null");
        final Note note = context.getNote();
        assertNotNull(note,"Note object is null");
        assertNotNull(note.getImage(),"Image object is null");
        assertNotNull(note.getText(),"Text is null");

    }
}