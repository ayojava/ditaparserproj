## How to run

- Clone the project and edit the following location on your system in the Main.java class

```java
private String inputFilePath ="/Users/ayodeji.ilori/Documents/intellijProj/DitaParserProj/src/main/resources/in/";

    private String outputFilePath ="/Users/ayodeji.ilori/Documents/intellijProj/DitaParserProj/src/main/resources/out/";
```

- Go  to MainTest.java file in the 'test' package , Run the test 'handleSample1()' method.

- test would pass and also display the parsed output of the Sample1.dita file

```java

Task(ditaArch=null, taskId=task.dita_ab1d07ad-a35a-42ee-9260-2eef3fa0c8b9, lang=null, ditaVersion=null, title=Check landing signalization, 
        taskBody=TaskBody(steps=[Step(cmd=Cmd(text=Check the signalization visually.  , 
        image=Image(placement=break, href=56da4222-ca76-469f-97cf-443eb3630f07.png))), 
        Step(cmd=Cmd(text=Push the call buttons.  , image=Image(placement=break, href=e2f15dec-f184-4e47-afe7-0efcd296abd4.png))), 
        Step(cmd=Cmd(text=Check that the buttons do not get stuck when pushed.  , image=Image(placement=break, href=224d2d67-c7bd-45b9-9031-0aede106136d.png)))]), 
        prolog=Prolog(metadata=[Othermeta(name=uuid, content=f4cbe2f6-6083-4495-819f-9f7d4e2bc4d4), 
        Othermeta(name=ft:clusterId, content=f4cbe2f6-6083-4495-819f-9f7d4e2bc4d4), 
        Othermeta(name=created, content=Wed Apr 21 2021 06:44:34 GMT+0000), Othermeta(name=dc:language, content=en-us), 
        Othermeta(name=path, content=/content/dam/tech-doc/en-us/testing_for_research_purposes/dmp/Check-landing-signalization.dita), 
        Othermeta(name=dita_class, content=- topic/topic task/task), Othermeta(name=state, content=Draft), 
        Othermeta(name=last_modified, content=Fri May 22 2020 11:56:57 GMT+0100), Othermeta(name=KTD_Title, content=Check landing signalization)]))

```